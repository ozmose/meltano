image: python:3.6
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache"

include:
  - "/docker/build-ci.yml"

stages:
  - lint
  - test
  - build_base
  - build
  - publish
  - review
  # - production

cache:
  paths:
    - .cache/pip
    - venv/

lint:
  stage: lint
  before_script:
    - pip install black
  script:
    - make show_lint

test:
  stage: test
  variables:
    POSTGRES_USER: runner
    POSTGRES_DB: pytest
    POSTGRES_PASSWORD: ""
    PG_ADDRESS: postgres
    PG_USERNAME: $POSTGRES_USER
    PG_PASSWORD: $POSTGRES_PASSWORD
  services:
    - postgres:latest
  before_script:
    - pip install '.[dev]'
  script:
    - pytest -v

.pages:
  script:
    - pip install sphinx sphinx-rtd-theme
    - cd doc ; make html
    - mv build/html/ ../public/
  artifacts:
    paths:
    - public
  only:
    - master

publish:
  stage: publish
  before_script:
    - pip install twine
  script:
    - python setup.py sdist
    - twine upload dist/*
  only:
    variables:
      - $CI_COMMIT_TAG

.docker_build_script: &docker_build_script |
  export DOCKER_IMAGE_NAME=${IMAGE_NAME:-$CI_REGISTRY_IMAGE}
  export DOCKER_IMAGE_TAG=${IMAGE_TAG:-$CI_COMMIT_SHA}
  docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  docker build -t $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG -f $DOCKERFILE .
  docker push $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG

.docker_build: &docker_build
  image: docker:latest
  stage: build
  variables:
    DOCKERFILE: .
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind
  script:
    - *docker_build_script

.review:
  image: silvs/kubectl:latest
  stage: review
  script:
    - echo ""
  only:
    - branches
  except:
    - master

###############
# Build Base  #
###############

# Manages:
#  - meltano/meltano/base:<sha>
meltano_base_dev:
  <<: *docker_build
  stage: build_base
  variables:
    DOCKERFILE: docker/base/Dockerfile
    IMAGE_NAME: $CI_REGISTRY_IMAGE/base
  except:
    - master
    - tags

# Manages:
#  - meltano/meltano/base:edge
meltano_base_edge:
  <<: *docker_build
  stage: build_base
  variables:
    DOCKERFILE: docker/base/Dockerfile
    IMAGE_NAME: $CI_REGISTRY_IMAGE/base
    IMAGE_TAG: edge
  only:
    - master

# Manages:
#  - meltano/meltano/base:<tag>
#  - meltano/meltano/base:latest
meltano_base:
  <<: *docker_build
  stage: build_base
  variables:
    DOCKERFILE: docker/base/Dockerfile
    IMAGE_NAME: $CI_REGISTRY_IMAGE/base
    IMAGE_TAG: $CI_COMMIT_TAG
  script:
    - *docker_build_script
    - docker tag $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG $DOCKER_IMAGE_NAME:latest
    - docker push $DOCKER_IMAGE_NAME:latest
  only:
    variables:
      - $CI_COMMIT_TAG

# Manages:
#  - meltano/meltano/cli:<sha>
meltano_cli_dev:
  <<: *docker_build
  stage: build_base
  variables:
    DOCKERFILE: docker/cli/Dockerfile
    IMAGE_NAME: $CI_REGISTRY_IMAGE/cli
  except:
    - master
    - tags

# Manages:
#  - meltano/meltano/cli:edge
meltano_cli_edge:
  <<: *docker_build
  stage: build_base
  variables:
    DOCKERFILE: docker/cli/Dockerfile
    IMAGE_NAME: $CI_REGISTRY_IMAGE/cli
    IMAGE_TAG: edge
  only:
    - master

# Manages:
#  - meltano/meltano/cli:<tag>
#  - meltano/meltano/cli:latest
meltano_cli:
  <<: *docker_build
  stage: build_base
  variables:
    DOCKERFILE: docker/cli/Dockerfile
    IMAGE_NAME: $CI_REGISTRY_IMAGE/cli
    IMAGE_TAG: $CI_COMMIT_TAG
  script:
    - *docker_build_script
    - docker tag $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG $DOCKER_IMAGE_NAME:latest
    - docker push $DOCKER_IMAGE_NAME:latest
  only:
    variables:
      - $CI_COMMIT_TAG

#############
# Build     #
#############

# Manages:
#  - meltano/meltano:<sha>
meltano_dev:
  <<: *docker_build
  variables:
    DOCKERFILE: docker/prod/Dockerfile
  before_script:
    # replace the FROM ... in the Dockerfile to use this branch's image
    - sed -i "1s#.*#FROM $CI_REGISTRY_IMAGE/base:$CI_COMMIT_SHA#" $DOCKERFILE
  except:
    - master
    - tags

# Manages:
#  - meltano/meltano:<sha>
#  - meltano/meltano:edge
meltano_edge:
  <<: *docker_build
  variables:
    DOCKERFILE: docker/prod/Dockerfile
  before_script:
    # replace the FROM ... in the Dockerfile to use this branch's image
    - sed -i "1s#.*#FROM $CI_REGISTRY_IMAGE/base:edge#" $DOCKERFILE
  script:
    - *docker_build_script
    - docker tag $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG $DOCKER_IMAGE_NAME:edge
    - docker push $DOCKER_IMAGE_NAME:edge
  only:
    - master
  except:
    variables:
      - $CI_COMMIT_TAG

# Manages:
#  - meltano/meltano:<tag>
#  - meltano/meltano:latest
meltano:
  <<: *docker_build
  variables:
    DOCKERFILE: docker/prod/Dockerfile
    IMAGE_TAG: $CI_COMMIT_TAG
  before_script:
    # replace the FROM ... in the Dockerfile to use this branch's image
    - sed -i "1s#.*#FROM $CI_REGISTRY_IMAGE/base#" $DOCKERFILE
  script:
    - *docker_build_script
    - docker tag $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG $DOCKER_IMAGE_NAME:latest
    - docker push $DOCKER_IMAGE_NAME:latest
  only:
    variables:
      - $CI_COMMIT_TAG

#############
# Publish   #
#############

# registry.gitlab.com/meltano/meltano:<tag> → docker.io/meltano:<tag>
# registry.gitlab.com/meltano/meltano:latest → docker.io/meltano:latest
hub_meltano: &docker_hub_publish
  image: docker:latest
  stage: publish
  services:
    - docker:dind
  variables:
    DOCKERFILE: .
    DOCKER_DRIVER: overlay2
    IMAGE_NAME: meltano/meltano
    IMAGE_TAG: $CI_COMMIT_TAG
    SOURCE_IMAGE: $CI_REGISTRY_IMAGE
  script:
    - docker pull $SOURCE_IMAGE:$IMAGE_TAG
    - docker pull $SOURCE_IMAGE:latest
    - docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD
    - docker tag $SOURCE_IMAGE:$IMAGE_TAG $IMAGE_NAME:$IMAGE_TAG
    - docker tag $SOURCE_IMAGE:latest $IMAGE_NAME:latest
    - docker push $IMAGE_NAME:$IMAGE_TAG
    - docker push $IMAGE_NAME:latest
  only:
    variables:
      - $CI_COMMIT_TAG

# registry.gitlab.com/meltano/meltano/cli:<tag> → docker.io/meltano/cli:<tag>
# registry.gitlab.com/meltano/meltano/cli:latest → docker.io/meltano/cli:latest
hub_meltano_cli:
  <<: *docker_hub_publish
  variables:
    IMAGE_NAME: meltano/cli
    IMAGE_TAG: $CI_COMMIT_TAG
    SOURCE_IMAGE: $CI_REGISTRY_IMAGE/cli

.production:
  image: silvs/kubectl:latest
  stage: production
  environment:
    name: production
    url: https://melt.gitlab-bizops.com
  script:
    - cd app/chart/meltano
    - helm init --client-only
    - helm dep update
    - helm upgrade --install --namespace default meltano . --set image.repository=$CI_REGISTRY_IMAGE/meltano,image.tag=base-$CI_COMMIT_SHA,ingress.enabled=true,'global.ingress.hosts[0]=melt.gitlab-bizops.com','global.ingress.tls[0].hosts[0]=melt.gitlab-bizops.com','global.ingress.tls[0].secretName=melt-tls',postgresql.postgresPassword=$POSTGRES_PASSWORD,oauth2-proxy.clientId=$OAUTH2_PROXY_CLIENT_ID,oauth2-proxy.clientSecret=$OAUTH2_PROXY_CLIENT_SECRET,oauth2-proxy.cookieSecret=$OAUTH2_PROXY_COOKIE_SECRET,oauth2-proxy.enabled=true,global.oauth2-proxy.enabled=true,oauth2-proxy.emailDomain=gitlab.com,oauth2-proxy.gitlabHost=https://dev.gitlab.org
  only:
    - master
